""" patent analysis tool developed by JohnTsai @ Delta Research Center.
Git Page: https://bitbucket.org/johntsai/patent/
Dependency: 
	package 'pattern' - http://www.clips.ua.ac.be/pattern
	package 'HTML' - www.decalage.info/python/html

Easy usage:
	[shell] python patent.py <input_filepath>
	[python interpreter]
	>>> from patent import patent
	>>> p = patent('filename')
	>>> p.run()


There are three important components (also target) of this tool:
1. Components
		Things that we guess they are components of the product mentioned in 
	this patent . I recognize them as strings in type "xxxx xxx 123", where 
	'xxx xxx' is component name, and the digits are component id.
		Note that patent.components return list of tuple (index, componentName) 
	instead of componentName. And there's no class for components because I 
	did not expect it to have any operation in advance.
2. Figures
		FIG. N.
		Figures and components that appear in the same paragraph will be 
	regarded as related ones. Class/Instance Figure maintains a list of all 
	components related to it.
3. Result
		Figures with only related components that appears in both description 
	and claim.
		In implementation, it is the same as class 'Figure'. And created by
	making a Figure called "_removeOthers()". 
	(Please attempt this by calling patent.getResult().)

"""

import simplejson as json
import re
import copy
import sys
import getopt
import urllib2
from pattern.en import tag, parsetree
import HTML
from lib import unique, lemmatize, HtmlBuilder, link2Top

class patent():

	""" Type is either 'file' or 'web', and the path must be filepath and url 
	respectively. 
	"""
	def __init__(self, type, path, outputPath=None):
		self.type = type
		self.outputPath = outputPath
		if type.lower() == "file":
			self.filepath = path
			self.loadFromFile(path)
		elif type.lower() == 'web':
			self.url = path
			self.loadFromWeb(path)
	
	def __repr__(self):
		if self.type == 'file':
			return "<patent class: '" + self.filepath + "'>"
		elif self.type == 'web':
			return "<patent class: '" + self.url + "'>"

	''' Use __repr__ directly.
	def __str__(self):
		return "patent class: '" + self.filepath + "'"
	'''
		
	""" Reset all results and read from self.raw"""
	def reset(self):
		# Reset results
		self.components = []
		self.figs = []
		self.componentsInBoth = []
		self.claim = None
		self.description = None
		self.descriptionLines = []
		self.results = []
		self.component2Fig = {}
		# Parse out claim and description
		self.description = self.raw['description'].replace("<BR>", "\n")
		self.descriptionLines = re.split(r'\n+', self.description)
		self.claim = self.raw['claim'].replace("<BR>", "\n")
	
	""" Load File and save the json dump to self.raw """
	def loadFromFile(self, filepath):
		with open(filepath,'r') as file:
			dump = json.load(file)
			response = dump['response']
			self.raw = response['docs'][0]
			self.reset()

	""" Read from url and save the json dump to self.raw """
	def loadFromWeb(self, url):
		# Set web opener
		authinfo = urllib2.HTTPBasicAuthHandler()
		authinfo.add_password(realm='Patent Login', 
			uri='http://172.16.76.230:9983', user='guest', passwd='guest123')
		proxy_support = urllib2.ProxyHandler(\
				{"http" : "http://172.16.76.230:9983"})
		opener = urllib2.build_opener(proxy_support, authinfo,\
											urllib2.CacheFTPHandler)
		fp = opener.open(url)
		dump = json.loads(fp.read())
		response = dump['response']
		self.raw = response['docs'][0]
		self.reset()

		
	""" Find all figures and the components that appear in the same paragraph.
	"""
	def findFigs(self, components=[]):
		if components == None or len(components)==0:
			components = self.components
			if components == None or len(components)==0:
				components = self.getComponents()
			
		# Find out all FIG and their position
		figPosition = {}
		# Find 2 in 'FIGS. 1 and 2.'
		pattern1 = re.compile('figs{0,1}. [0-9]+[a-z\-0-9]* and ([0-9][a-z\-0-9]*)', re.I)
		pattern2 = re.compile('figs{0,1}. [0-9]+[a-z\-0-9]*', re.I)
		for x in xrange(0, len(self.descriptionLines)):
			results = re.findall(pattern1, self.descriptionLines[x]) + re.findall(pattern2, self.descriptionLines[x])
			for y in results:
				# Merge FIG. 1 and FIGS. 1 -> FIG. 1
				if re.match(r'[0-9]', y):
					y = 'FIG. ' + y
				elif y[:4].upper() == 'FIGS':
					y = y[:3] + y[4:]
				y = y.upper()
				if not figPosition.has_key(y):
					figPosition[y] = []
				figPosition[y].append(x)	
		print "All figs found."
		
		# Put FIGs and components in the same paragraph together
		figs = []
		for name in figPosition.iterkeys():
			fig = Figure(name)
			componentPtr = 0
			for pos in figPosition[name]:
				print "search " + str(pos) + " - " 
				while(componentPtr < len(components)):
					print componentPtr,
					print " -> ",
					print components[componentPtr][0],
					print " : " + components[componentPtr][1]
					if(components[componentPtr][0] == pos):
						componentName = components[componentPtr][1]
						fig.addComponent(componentName)
						if not self.component2Fig.has_key(componentName):
							self.component2Fig[componentName] = []
						self.component2Fig[componentName].append(fig)
					elif(components[componentPtr][0] > pos):
						break
					componentPtr += 1
			fig.show()
			figs.append(fig)
		self.figs = figs
		return figs
		
	def getComponents(self):
		pattern = re.compile("(the|a|an) ([a-z][a-z\- \']*? [0-9]+)", re.I)
		results = []
		for x in xrange(0, len(self.descriptionLines)):
			_result = re.findall(pattern, self.descriptionLines[x])
			for y in xrange(0, len(_result)):
				value = _result[y][1]
				results.append( (x, value) )
				
		components = []
		for id, value in results:
			keep = True
			# Make sure that the result matches the shortest one
			newValue = re.findall(pattern, value)
			while len(newValue) > 0:
				value = newValue[0][1]
				newValue = re.findall(pattern, value)

			# Remove those with more than 7 words 
			if len(re.split('\W+',value)) > 7:
				keep = False
				continue
			# Remove those with wrong POS tag. That is: 
			# 'blah blah of 123' -> remove
			# 'blah blah is 123' -> remove
			# 'blah blah to 123' -> remove
			tagged = tag(value)
			lastword = tagged[len(tagged) -2]
			if lastword[1] == 'IN' or lastword[1][0:2] == 'VB' or lastword[1] == 'TO' or lastword[1] == 'CC':
				keep = False
				continue
			for word, pos in tagged:
				if pos == 'IN':
					keep = False
			if keep:
				components.append((id, value))
				
		# Remove duplicate ones and return. 
		components = list(set(components))
		components.sort()
		self.components = components
		return components

	""" 
	1. Search and return all queries in list argument [allQuery] that appear
	in claim.
	2. searchInClaim() IS getComponentsInBoth()
		If called without any arguments, it'll get components from description
	and remove the id of them, then use them as [allQuery]. 
		That is, get components in both description and claim.
	"""
	def searchInClaim(self, allQuery=None):
		results = []
		autoMode = False
		if allQuery == None or len(allQuery) == 0:
			# Prevent that components are not computed yet
			allQuery = []
			autoMode = True
			if len(self.components) == 0 or self.components == None:
				self.getComponents()
			for id, x in self.components:
				y = re.sub(r' *[0-9]+', '', x)
				allQuery.append(y)
		if type(allQuery) is not list:
			print "Error: [allQuery] argument must be type 'list'."
			return None
		
		for x in allQuery:
			lemma = lemmatize(x)
			if x in self.claim or lemma in self.claim:
				results.append(str(x))
				if x <> lemma:
					results.append(str(lemma))
				
		if len(results)==0:
			print "Nothing found."
		else:
			results = unique(results)
		if autoMode:
			self.componentsInBoth = results
		return results
		
	""" Unused """
	def findComponentsByID(self, id):
		if len(self.components) == 0:
			self.getComponents()
		results = set([])
		for line, text in self.components:
			textID = re.search(r'\d+',text).group()
			if id == textID:
				results.add(text)
		return list(results)
		
	""" Find all FIGs with components which appear in both description and 
		claim.
	"""
	def getResult(self):
		if self.figs == None or len(self.figs)==0:
			self.figs = self.findFigs()
		# Prevent modifying global figs
		__figs = copy.deepcopy(self.figs)
		if self.componentsInBoth == None or len(self.componentsInBoth)==0:
			self.componentsInBoth = self.searchInClaim()
		results = []
		for fig in __figs:
			fig._removeOthers(self.componentsInBoth)
			if len(fig.components)>0:
				results.append(fig)
		self.results = results
		return results
	
	""" Print out the Result """
	def showResult(self):
		if len(self.results) == 0:
			self.getResult()
		if len(self.components) == 0:
			self.getComponents()
		for fig in self.results:
			fig.showSameID(self.components)
		return copy.deepcopy(self.results)
	
	""" Print out all Figures related to the input component """
	def showFigOfComponent(self, component):
		try:
			figs = []
			for fig in self.component2Fig[component]:
				figs.append(str(fig.name))
			print figs
			return self.component2Fig[component]
		except KeyError:
			return []
			
	""" Run all the functions and output the results into outputPath, or file\
	 "[filepath].out" when outputPath is not set.
	"""
	def run(self, outputPath=None):
		if outputPath == None:
			# Then get outputPath from self attribute
			if self.outputPath == None and self.type == 'file':
				outputPath = self.filepath + ".out"
			else:
				outputPath = self.outputPath
		with open(outputPath, 'w') as fh:
			self.reset()

			# 1. Components
			components = self.getComponents()
			uniqueComponents = set([])
			for id, text in components:
				uniqueComponents.add(text)
			fh.write(">>> Components: (%d unique results)\n" %len(uniqueComponents))
			for component in uniqueComponents:
				fh.write(component + "\n")

			# 2. Components in Both
			componentsInBoth = self.searchInClaim()
			fh.write("\n>>> Components in both Description and Claim: \
								(%d results)\n" %len(componentsInBoth))
			for component in componentsInBoth:
				fh.write(component + "\n")

			# 3. Figures
			figs = self.findFigs()
			fh.write("\n>>> Figures: (%d results)\n" %len(figs))
			for fig in figs:
				fh.write(str(fig))

			# 4. Components 2 Figure
			fh.write('\n>>> Components to figures\n')
			for component in uniqueComponents:
				fh.write(component + " - \n")
				try:
					for fig in self.component2Fig[component]:
						fh.write("\t%s\n" %fig.name)
				except KeyError:
					fh.write("\t[]\n")
					
			# 5. Results
			results = self.getResult()
			fh.write("\n>>> Results: (%d results)\n" %len(results))
			for res in results:
				fh.write(res.getSameID(components))

	""" Run all the functions and output into a html web page. """
	def createHtml(self, title=None, outputPath=None):
		if title == None:
			if self.type == 'file':
				title = "Patent Analysis Result of %s" %self.filepath
			elif self.type == 'web':
				title = "Patent Analysis Result of %s" %self.url
		header = "Generated by 'patent' analysis tool developed by JohnTsai @\
Delta Research Center."

		hb = HtmlBuilder(title, header=header)
		self.reset()
		# 0. Link to Different Part of Result
		hb.append('<a name="Top">')
		hb.append("<h1>" + title + "</h1>\n")
		hb.append("<h2>Over View and Link</h2>\n")
		titleList = [('Components', 					'#Components'),
		('Components in Both Description and Claim', 	'#ComponentsInBoth'),
		('Figures', 									'#Figures'),
		('Components to Related Figures Table', 		'#Component2Figure'),
		('Results', 									'#Results'),
		]
		linkList= []
		for text, anchor in titleList:
			linkList.append(HTML.link(text, anchor))
		htmlcode = HTML.list(linkList)
		hb.append(htmlcode)
		
		# 1. Components
		hb.append('<div id="Components">\n')
		components = self.getComponents()
		uniqueComponents = set([])
		for id, text in components:
			uniqueComponents.add(text)
		link = link2Top("Components: (%d unique results)" 
										%len(uniqueComponents))
		hb.append("<h2>" + link + "</h2>\n")
										
		htmlcode = HTML.list(list(uniqueComponents))
		hb.append(htmlcode)
		hb.append('</div>')

		# 2. Components in Both
		hb.append('<div id="ComponentsInBoth">\n')
		componentsInBoth = self.searchInClaim()
		link = link2Top("Components in both Description and Claim: \
								(%d results)" %len(componentsInBoth))
		hb.append("<h2>" + link + "</h2>\n")
		htmlcode = HTML.list(componentsInBoth)
		hb.append(htmlcode)
		hb.append('</div>')

		# 3. Figures
		hb.append('<div id="Figures">\n')
		figs = self.findFigs()
		link = link2Top("Figures: (%d results)" %len(figs))
		hb.append("<h2>" + link + "</h2>\n")
		t = HTML.Table(header_row=['Figure', 'Components'])
		for fig in figs:
			componentList = HTML.list(list(fig.components))
			t.rows.append([fig.name, componentList])
		htmlcode = str(t)
		hb.append(htmlcode)
		hb.append('</div>')

		# 4. Components 2 Figure
		hb.append('<div id="Component2Figure">\n')
		link = link2Top("Components to figures")
		hb.append("<h2>" + link + "</h2>\n")
		t = HTML.Table(header_row=['Component', 'Figures'],
			col_align=['right', 'center'])
		for component in uniqueComponents:
			try:
				figNames = set([])
				for fig in self.component2Fig[component]:
					figNames.add(fig.name)
				figList = HTML.list(list(figNames))
			except KeyError:
				figList = "None"
			t.rows.append([component, figList])
		htmlcode = str(t)
		hb.append(htmlcode)
		hb.append('</div>')
				
		# 5. Results
		hb.append('<div id="Results">\n')
		results = self.getResult()
		link = link2Top('Figures and Related Components in Both \
				Description and Claim (%s results)' %len(results))
		hb.append("<h2>" + link + "</h2>\n")
		t = HTML.Table(header_row=['Figure', 'Components'], 
			col_width=['10%', ''],
			width='80%')
		for res in results:
			t.rows.append(res.getResultTable(components))
		hb.append(str(t))
		hb.append('</div>')
		
		if outputPath == None:
			if self.outputPath == None and self.type == 'file':
				outputPath = re.sub('\..*$','.htm',self.filepath)
			else:
				outputPath = self.outputPath
		try:
			print "Writing to %s...\n" %outputPath
			fh = open(outputPath, 'w')
		except TypeError:
			print "outputPath not defined."
		except IOError:
			print "Cannot write file %s" %outputPath
		else:
			fh.write(hb.getResult())
		return hb.getResult()
				
					
""" Instance that hold FIG name and a components set """
class Figure():
	
	def __init__(self, name):
		self.name = name
		self.components = set([])
		
	def __str__(self):
		str_ = self.name + " - \n"
		for x in self.components:
			str_ += "\t" + x + "\n"
		return str_
		
	def __repr__(self):
		return "instance_Figure(" + self.name + ")"
		
	def addComponent(self, component):
		try:
			self.components.add(str(component))
		except:
			print "Cannot add component: ",
			print component,
			print "It cannot be convert into string"
			
	def _removeOthers(self, keepList):
		removeList = []
		for x in self.components:
			keep = False
			for y in keepList:
				matched = re.match(y, x, re.I)
				if matched <> None:
					keep = True
			if not keep:			
				removeList.append(x)
				print "removing " + x
		for x in removeList:
			self.components.remove(x)
		return self.components

	""" Show figure name and the components related to it, while splitting all
	components to different lines.
	"""
	def show(self):
		print self.name + " - "
		for x in self.components:
			print "\t" + x
	
	""" Show figure name and the components related to it, while merging the
	components with same it into a line.
	"""
	def showSameID(self, components):
		print self.name + " - "
		ids = set([])
		for component in self.components:
			ids.add(re.search(r'\d+', component).group())
		for id in ids:
			print "\t",
			for component in self.findComponentsByID(id, components):
				print component + ", ",
			print ""

	def findComponentsByID(self, id, components):
		results = set([])
		for line, text in components:
			textID = re.search(r'\d+',text).group()
			if id == textID:
				results.add(text)
		return list(results)
		
	def getSameID(self, components):
		text = self.name + " - \n"
		ids = set([])
		for component in self.components:
			ids.add(re.search(r'\d+', component).group())
		for id in ids:
			text += "\t"
			for component in self.findComponentsByID(id, components):
				text += component + ", "
			text += "\n"
		return text

	def getResultTable(self, components):
		ids = set([])
		groupedComponents = []
		for component in self.components:
			ids.add(re.search(r'\d+', component).group())
		for id in ids:
			text = ""
			for component in self.findComponentsByID(id, components):
				text += component + ", "
			groupedComponents.append(text)
		return [self.name, HTML.list(groupedComponents)]
	
def shortUsage():
	print "Invalid Arguments\n\
Usage: python patent.py -i <input_filepath> [--plain] [--web] [--all] [-o <output_path>] "


def main(argv):
	runPlain = False
	runWeb = False
	outputPath = None
	try:
		opts, args = getopt.getopt(argv, "pwi:ao:", ['plain', 'web', 'all', 'out='])
	except getopt.GetoptError:
		shortUsage()
		sys.exit(2)
	for opt, arg in opts:
		if opt in ("-w", "--web"):
			runWeb = True
		elif opt in ("-p", "--plain"):
			runPlain = True
		elif opt == '-i':
			filepath = arg
		elif opt in ('-o', '--out'):
			outputPath = arg
			
	if type(filepath) is str:
		if not runPlain and not runWeb:
			runPlain = True
		elif runPlain and runWeb and outputPath <> None:
			print "Please select to output html or plaintext to %s"%outputPath
		p = patent('file', filepath, outputPath=outputPath)
		if runPlain:
			p.run()
		if runWeb:
			p.createHtml()
	else:
		shortUsage()
		sys.exit(2)

		
# Auto run Script
if __name__ == '__main__':
	if len(sys.argv)>1:
		main(sys.argv[1:])
	else:
		shortUsage()
		sys.exit(2)
else:
	print "\nPatent Analysis Tool by JohnTsai\n\
Easy Usage: \n\
\t[shell] python patent.py <input_filepath>\n\
\t[python interpreter]\n\
\t\t>>> from patent import patent\n\
\t\t>>> p = patent('filename')\n\
\t\t>>> p.run()		# if you want plaintext result\n\
\t\t>>> p.createHtml()	# if you want html result\n\
"
