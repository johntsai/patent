from pattern.en import parsetree

def lemmatize(input):
	# Make nouns singular 
	s = parsetree(input, lemmata=True)
	lemma = ""
	for sentence in s:
		for word in sentence.words:
			lemma += word.lemma + " "
	return lemma[:len(lemma)-1]		# Remove the " "

""" From: http://www.peterbe.com/plog/uniqifiers-benchmark """
def unique(seq, idfun=None): 
   # order preserving
   if idfun is None:
	   def idfun(x): return x
   seen = {}
   result = []
   for item in seq:
	   marker = idfun(item)
	   # in old Python versions:
	   # if seen.has_key(marker)
	   # but in new ones:
	   if marker in seen: continue
	   seen[marker] = 1
	   result.append(item)
   return result
		
def link2Top(text):
    return '<a title="Click to go to top" href="#Top">%s</a>' % (text)

		
''' Replaced by open() and HtmlBuilder() now '''
class HtmlWriter():
	def __init__(self, filename, title, header=''):
		self.filename = filename
		self.title = title
		self.header = header
	
	def __enter__(self):
		self.fh = open(self.filename, 'w')
		self.fh.write("<!--" + self.header + "-->\n")
		self.fh.write('''<html>
<head><title>%s</title></head>
<body>''' 
		% self.title)
		return self.fh
	
	def __exit__(self, type, value, traceback):
		self.fh.write('</body></html>')
		self.fh.close()
		return False

class HtmlBuilder():
	def __init__(self, title, header=''):
		self.title = title
		self.header = header
		buffer = []
		buffer.append("<!--" + self.header + "-->\n")
		buffer.append('''<html>
<head><title>%s</title></head>
<body>''' % self.title)
		self.buffer = buffer
	
	def __repr__(self):
		return "<HtmlBuilder: %s>"%self.title
	
	def __str__(self):
		return "".join(self.buffer) + "</body></html>"
	
	def append(self, new):
		try:
			self.buffer.append(str(new))
		except:
			print "Not appended"
	
	def getResult(self):
		return str(self)
	